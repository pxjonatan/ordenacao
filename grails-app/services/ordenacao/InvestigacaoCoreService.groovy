package ordenacao

import grails.transaction.Transactional

@Transactional
class InvestigacaoCoreService {

    def novaInvestigacao(int altura = 800, int largura = 600) {
        def crime = new Ponto (x:(new Random().nextInt(altura+1)),y:new Random().nextInt(largura+1)).save(flush:true)
        def inves = new InvestigacaoCore(crime:crime, altura:altura, largura:largura).save(flush:true);

        //Quadrilha1
        def quadrilha1 = new Quadrilha (nome:"BastardsBlood")
        def quadrilha2 = new Quadrilha (nome:"BlackDeath")
        (0..(new Random().nextInt(40))+20).each {
            def ponto1 = new Ponto (x:(new Random().nextInt(altura+1)),y:new Random().nextInt(largura+1)).save(flush:true)
            def ponto2 = new Ponto (x:(new Random().nextInt(altura+1)),y:new Random().nextInt(largura+1)).save(flush:true)

            quadrilha1.addToPontos(ponto1)
            quadrilha2.addToPontos(ponto2)
        }
        quadrilha1.save(flush:true)
        quadrilha2.save(flush:true)

        inves.addToQuadrilhas(quadrilha1)
        inves.addToQuadrilhas(quadrilha2)

        inves.save(flush:true)
    }

    def analisarCrime(def investigacaoCoreInstance){
        def listDistancias = []
        investigacaoCoreInstance?.quadrilhas?.each { quad ->
            quad.pontos.each { dot ->
                def distancia = Math.sqrt(Math.pow(dot.x-investigacaoCoreInstance?.crime.x,2)+Math.pow(dot.y-investigacaoCoreInstance?.crime.y,2))

                def map = [
                    quadrilha:quad,
                    ponto:dot,
                    distancia:distancia
                ]

                listDistancias.add(map)

            }
        }
        [crime:investigacaoCoreInstance?.crime,distancias:quickSort(listDistancias)]
    }


    def quickSort(List list) {
        if (list.isEmpty())
            return list
        def anItem = list[0]
        def smallerItems = list.findAll{it.distancia < anItem.distancia}
        def equalItems = list.findAll{it.distancia == anItem.distancia}
        def largerItems = list.findAll{it.distancia > anItem.distancia}
        quickSort(smallerItems) + equalItems + quickSort(largerItems)
    }
}
