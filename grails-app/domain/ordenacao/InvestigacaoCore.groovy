package ordenacao

class InvestigacaoCore {
    int altura
    int largura
    Ponto crime
    static hasMany = [quadrilhas:Quadrilha]


    String toString() {
        "Crime: (${crime?.x}, ${crime?.y}), Quadrilhas: ${quadrilhas?.size()}"
    }

    static constraints = {
    }
}
