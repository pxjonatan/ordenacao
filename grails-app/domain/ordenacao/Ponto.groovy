package ordenacao

class Ponto {
    String name
    int x
    int y

    String getName() {
        "($x, $y)"
    }

    String toString() {
        "($x, $y)"
    }

    static ArrayList<Ponto> newList() {
        ArrayList<Ponto> pontos = new ArrayList<Ponto>()
        ArrayList<Quadrilha> quads = Quadrilha.list()
        quads.each { 	pontos.addAll(it.pontos) }
        pontos = Ponto.list()-pontos
    }
}
