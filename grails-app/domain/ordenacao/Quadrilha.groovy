package ordenacao

class Quadrilha {
    String nome
    static hasMany = [ pontos:Ponto]

    String toString(){
        "$nome (${pontos?.size()} pontos)"
    }

    static constraints = {
    }

    static ArrayList<Quadrilha> newList(){
        ArrayList<Quadrilha> quads = new ArrayList<Quadrilha>();
        ArrayList<InvestigacaoCore> inves = InvestigacaoCore.list()
        inves.each {
            quads.addAll(it.quadrilhas)
        }

        (Quadrilha.list()-quads)
    }

}
