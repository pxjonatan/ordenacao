<div id="create-ponto" class="content scaffold-create" role="main">
	<h1><g:message code="default.create.label" args="['Ponto']" /></h1>
		<fieldset class="form">
			<div class="fieldcontain required">
				<label for="x">
					<g:message code="ponto.x.label" default="X" />
					<span class="required-indicator">*</span>
				</label>
				<g:field min="0" max="400" id="pontoX" name="x" type="number" value="" required=""/>
			</div>
			<div class="fieldcontain required">
				<label for="y">
					<g:message code="ponto.y.label" default="Y" />
					<span class="required-indicator">*</span>
				</label>
				<g:field min="0" max="400"  id="pontoY" name="y" type="number" value="" required=""/>
			</div>
		</fieldset>
		<fieldset class="buttons">
			<input type="button" name="createPonto" value="Criar Ponto" class="save" onclick="savePonto()">
		</fieldset>
</div>
