<%@ page import="ordenacao.Ponto" %>



<div class="fieldcontain ${hasErrors(bean: pontoInstance, field: 'x', 'error')} required">
	<label for="x">
		<g:message code="ponto.x.label" default="X" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="x" type="number" value="${pontoInstance.x}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: pontoInstance, field: 'y', 'error')} required">
	<label for="y">
		<g:message code="ponto.y.label" default="Y" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="y" type="number" value="${pontoInstance.y}" required=""/>

</div>

