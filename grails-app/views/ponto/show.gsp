	<h1>Ver Ponto:</h1>
		<div id="show-ponto" class="content scaffold-show" role="main">
			<ol class="property-list ponto">
				{<g:if test="${pontoInstance?.x}">
					<span id="x-label" class="property-label"><g:message code="ponto.x.label" default="X :" /></span>
						<span class="property-value" aria-labelledby="x-label"><g:fieldValue bean="${pontoInstance}" field="x"/></span>
				</g:if>,
				<g:if test="${pontoInstance?.y}">
					<span id="y-label" class="property-label"><g:message code="ponto.y.label" default="Y :" /></span>
						<span class="property-value" aria-labelledby="y-label"><g:fieldValue bean="${pontoInstance}" field="y"/></span>
				</g:if>}
			</ol>
		</div>
