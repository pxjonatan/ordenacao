<%@ page import="ordenacao.*" %>
<%@ page import grails.converters.JSON %>
		<div id="create-quadrilha" class="content scaffold-create" role="main">
			<h1><g:message code="default.create.label" args="['Quadrilha']" /></h1>
			<g:form url="[resource:quadrilhaInstance, action:'save']" >
				<fieldset class="form">
					<div class="fieldcontain ${hasErrors(bean: quadrilhaInstance, field: 'nome', 'error')} required">
						<label for="nome">
							<g:message code="quadrilha.nome.label" default="Nome" />
							<span class="required-indicator">*</span>
						</label>
						<g:textField id="quad-nome" name="nome" required="" value=""/>

					</div>

					<div class="fieldcontain ${hasErrors(bean: quadrilhaInstance, field: 'pontos', 'error')} ">
						<label for="pontos">
							<g:message code="quadrilha.pontos.label" default="Pontos" />

						</label>

						<g:select id="quad-pontos" name="pontos" from="${ordenacao.Ponto.newList()}" multiple="multiple" optionKey="id" optionValue="name" size="5" value="${quadrilhaInstance?.pontos*.id}" class="many-to-many"/>
					</div>
				</fieldset>
				<fieldset class="buttons">
					<input type="button" name="createQuad" value="Criar Quadrilha" class="save" onclick="saveQuad()">
				</fieldset>
			</g:form>
		</div>
