<%@ page import="ordenacao.Quadrilha" %>



<div class="fieldcontain ${hasErrors(bean: quadrilhaInstance, field: 'nome', 'error')} required">
	<label for="nome">
		<g:message code="quadrilha.nome.label" default="Nome" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="nome" required="" value="${quadrilhaInstance?.nome}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: quadrilhaInstance, field: 'pontos', 'error')} ">
	<label for="pontos">
		<g:message code="quadrilha.pontos.label" default="Pontos" />
		
	</label>
	<g:select name="pontos" from="${ordenacao.Ponto.list()}" multiple="multiple" optionKey="id" size="5" value="${quadrilhaInstance?.pontos*.id}" class="many-to-many"/>

</div>

