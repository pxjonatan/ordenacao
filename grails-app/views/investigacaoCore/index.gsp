
<%@ page import="ordenacao.InvestigacaoCore" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'investigacaoCore.label', default: 'InvestigacaoCore')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-investigacaoCore" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-investigacaoCore" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="altura" title="${message(code: 'investigacaoCore.altura.label', default: 'Altura')}" />
					
						<th><g:message code="investigacaoCore.crime.label" default="Crime" /></th>
					
						<g:sortableColumn property="largura" title="${message(code: 'investigacaoCore.largura.label', default: 'Largura')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${investigacaoCoreInstanceList}" status="i" var="investigacaoCoreInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${investigacaoCoreInstance.id}">${fieldValue(bean: investigacaoCoreInstance, field: "altura")}</g:link></td>
					
						<td>${fieldValue(bean: investigacaoCoreInstance, field: "crime")}</td>
					
						<td>${fieldValue(bean: investigacaoCoreInstance, field: "largura")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${investigacaoCoreInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
