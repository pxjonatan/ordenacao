		<div id="create-investigacaoCore" class="content scaffold-create" role="main">
			<h1>Criar Investigação</h1>
				<fieldset class="form">

					<div class="fieldcontain ${hasErrors(bean: investigacaoCoreInstance, field: 'crime', 'error')} required">
						<label for="crime">
							<g:message code="investigacaoCore.crime.label" default="Crime" />
							<span class="required-indicator">*</span>
						</label>
						<g:select id="crime-crime" name="crime.id" from="${ordenacao.Ponto.newList()}" optionKey="id" optionValue="name" required="" value="" class="many-to-one"/>

					</div>


					<div class="fieldcontain ${hasErrors(bean: investigacaoCoreInstance, field: 'quadrilhas', 'error')} ">
						<label for="quadrilhas">
							<g:message code="investigacaoCore.quadrilhas.label" default="Quadrilhas" />

						</label>
						<g:select id="crime-quad" name="quadrilhas" from="${ordenacao.Quadrilha.newList()}" multiple="multiple" optionKey="id" optionValue="nome" size="5" value="" class="many-to-many"/>

					</div>
				</fieldset>
				<fieldset class="buttons">
					<input type="button" name="createCrime" value="Criar Investigação" class="save" onclick="saveCrime()">
				</fieldset>
			</div>
