
<%@ page import="ordenacao.InvestigacaoCore" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'investigacaoCore.label', default: 'InvestigacaoCore')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-investigacaoCore" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-investigacaoCore" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list investigacaoCore">
			
				<g:if test="${investigacaoCoreInstance?.altura}">
				<li class="fieldcontain">
					<span id="altura-label" class="property-label"><g:message code="investigacaoCore.altura.label" default="Altura" /></span>
					
						<span class="property-value" aria-labelledby="altura-label"><g:fieldValue bean="${investigacaoCoreInstance}" field="altura"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${investigacaoCoreInstance?.crime}">
				<li class="fieldcontain">
					<span id="crime-label" class="property-label"><g:message code="investigacaoCore.crime.label" default="Crime" /></span>
					
						<span class="property-value" aria-labelledby="crime-label"><g:link controller="ponto" action="show" id="${investigacaoCoreInstance?.crime?.id}">${investigacaoCoreInstance?.crime?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
				<g:if test="${investigacaoCoreInstance?.largura}">
				<li class="fieldcontain">
					<span id="largura-label" class="property-label"><g:message code="investigacaoCore.largura.label" default="Largura" /></span>
					
						<span class="property-value" aria-labelledby="largura-label"><g:fieldValue bean="${investigacaoCoreInstance}" field="largura"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${investigacaoCoreInstance?.quadrilhas}">
				<li class="fieldcontain">
					<span id="quadrilhas-label" class="property-label"><g:message code="investigacaoCore.quadrilhas.label" default="Quadrilhas" /></span>
					
						<g:each in="${investigacaoCoreInstance.quadrilhas}" var="q">
						<span class="property-value" aria-labelledby="quadrilhas-label"><g:link controller="quadrilha" action="show" id="${q.id}">${q?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:investigacaoCoreInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${investigacaoCoreInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
