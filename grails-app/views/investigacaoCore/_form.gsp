<%@ page import="ordenacao.InvestigacaoCore" %>



<div class="fieldcontain ${hasErrors(bean: investigacaoCoreInstance, field: 'altura', 'error')} required">
	<label for="altura">
		<g:message code="investigacaoCore.altura.label" default="Altura" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="altura" type="number" value="${investigacaoCoreInstance.altura}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: investigacaoCoreInstance, field: 'crime', 'error')} required">
	<label for="crime">
		<g:message code="investigacaoCore.crime.label" default="Crime" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="crime" name="crime.id" from="${ordenacao.Ponto.list()}" optionKey="id" required="" value="${investigacaoCoreInstance?.crime?.id}" class="many-to-one"/>

</div>

<div class="fieldcontain ${hasErrors(bean: investigacaoCoreInstance, field: 'largura', 'error')} required">
	<label for="largura">
		<g:message code="investigacaoCore.largura.label" default="Largura" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="largura" type="number" value="${investigacaoCoreInstance.largura}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: investigacaoCoreInstance, field: 'quadrilhas', 'error')} ">
	<label for="quadrilhas">
		<g:message code="investigacaoCore.quadrilhas.label" default="Quadrilhas" />
		
	</label>
	<g:select name="quadrilhas" from="${ordenacao.Quadrilha.list()}" multiple="multiple" optionKey="id" size="5" value="${investigacaoCoreInstance?.quadrilhas*.id}" class="many-to-many"/>

</div>

