<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		<title>Simulador de Investigação</title>
		<style type="text/css" media="screen">

		caption {
		    display: block;
		    line-height: 3em;
		    width: 100%;
		    border: 1px solid #eee;
		}
		caption h1 {
			positio: relative;
			text-align: center;
		}

		       .flexy {
		            display: block;
		            width: 100%;
		            border: 1px solid #eee;
		            max-height: 320px;
		            overflow: auto;
		        }

		        .flexy thead {
		            display: -webkit-flex;
		            -webkit-flex-flow: row;
		        }

		        .flexy thead tr {
		            padding-right: 15px;
		            display: -webkit-flex;
		            width: 100%;
		            -webkit-align-items: stretch;
		        }

		        .flexy tbody {
		            display: -webkit-flex;
		            height: 100px;
		            overflow: auto;
		            -webkit-flex-flow: row wrap;
		        }
		        .flexy tbody tr{
		            display: -webkit-flex;
		            width: 100%;
		        }

		        .flexy tr td {
		            width: 15%;
		        }
			#status {
				background-color: #eee;
				border: .2em solid #fff;
				margin: 2em 2em 1em;
				padding: 1em;
				width: 12em;
				float: left;
				-moz-box-shadow: 0px 0px 1.25em #ccc;
				-webkit-box-shadow: 0px 0px 1.25em #ccc;
				box-shadow: 0px 0px 1.25em #ccc;
				-moz-border-radius: 0.6em;
				-webkit-border-radius: 0.6em;
				border-radius: 0.6em;
			}

			.ie6 #status {
				display: inline; /* float double margin fix http://www.positioniseverything.net/explorer/doubled-margin.html */
			}

			#status ul {
				font-size: 0.9em;
				list-style-type: none;
				margin-bottom: 0.6em;
				padding: 0;
			}

			#status li {
				line-height: 1.3;
			}

			#status h1 {
				text-transform: uppercase;
				font-size: 1.1em;
				margin: 0 0 0.3em;
			}

			#page-body {
				margin: 2em 1em 1.25em 18em;
			}

			h2 {
				margin-top: 1em;
				margin-bottom: 0.3em;
				font-size: 1em;
			}

			p {
				line-height: 1.5;
				margin: 0.25em 0;
			}

			#controller-list ul {
				list-style-position: inside;
			}

			#controller-list li {
				line-height: 1.3;
				list-style-position: inside;
				margin: 0.25em 0;
			}

			@media screen and (max-width: 480px) {
				#status {
					display: none;
				}

				#page-body {
					margin: 0 1em 1em;
				}

				#page-body h1 {
					margin-top: 0;
				}
			}

			a{
				cursor: pointer;
				display: block;
				padding-bottom: 5px;
			}
			hr {
				margin-top: 15px;
				margin-bottom: 15px;
			}


			.done{
				text: black;
			}
			#none {
				display:none;
			}

			#div-quad, #div-ponto, #div-crime {
				display: inline-block;
			}

		</style>
	</head>
	<body>
		<div id="status" role="complementary">
			<h1>Ações:</h1>
			<ol>
				<li>
					<a onclick="newPonto()">Novo Ponto</a>
				</li>
				<li>
					<a onclick="newQuad()">Nova Quadrilha</a>
				</li>
				<li>
					<a onclick="newCrime()">Nova Investigação</a>
				</li>
				<li>
					<a onclick="investigarCrime()">Investigar Crime</a>
				</li>
				<hr>
				<li>
					<a onclick="limparBanco();">Zerar Registros</a>
				</li>
				<li>
					<a onclick="autoIt();">Criação Automática</a>
				</li>
			</ol>
		</div>
		<div id="none"></div>
		<div id="page-body" role="main">
		</div>
		<div id="page-footer" align='center'>
			<div id="div-ponto" style="width:33%"></div>
			<div id="div-quad" style="width:33%"></div>
			<div id="div-crime" style="width:33%"></div>
		</div>
		<div id="page-canvas" align="center">
			<canvas id="myCanvas" width="800" height="600" style="border:1px solid #000000;"></canvas>
		</div>

		<script type="text/javascript" src="./js/ordenacao/p5.min.js" ></script>
		<script type="text/javascript">
		var self = this;
		var pointSize = 10;
		var id = 0;
		var color = '';
		var autoIt = function(){
			$.ajax({
				url: "./investigacaoCore/autoIt",
				success: function (data){
					console.log(data);
					drawPoint(data.crime, 0);
					for (var i in data.distancias){
						drawPoint(data.distancias[i], ++i);
					}
				}
			}).done(function() {
				$.ajax({
					url: "./ponto/index.json",
					method: "POST",
					context: $("#div-ponto"),
					success: function (data){
						var head = "<div style='width:100%'><caption><h1>Listagem de Pontos</h1></caption><table class='flexy' ><thead><tr><td>X</td><td>Y</td></tr></thead><tbody>";
						var body = '';
						var footer = "</tbody></table></div>"
						for (var i in data) {
							body += "<tr><td>"+data[i].x+"</td><td>"+data[i].y+"</td><tr>"
						}
						var table = head+body+footer;
						$(this).html(table);
					}
				}).done(function() {
					$.ajax({
						url: "./quadrilha/index.json",
						method: "POST",
						context: $("#div-quad"),
						success: function (data){
							var head = "<div style='width:100%'><caption><h1>Listagem de Quadrilhas</h1></caption><table class='flexy' ><thead><tr><td>Nome</td><td>Pontos</td></tr></thead><tbody>";
							var body = '';
							var footer = "</tbody></table></div>"
							for (var i in data) {
								var pontos = data[i].pontos;
								body += "<tr><td>"+data[i].nome+"</td><td>"+pontos.length+"</td><tr>"
							}
							var table = head+body+footer;
							$(this).html(table);
						}
					}).done(function() {
						$.ajax({
							url: "./investigacaoCore/index.json",
							method: "POST",
							context: $("#div-crime"),
							success: function (data){
								var head = "<div style='width:100%'><caption><h1>Listagem de Investigações</h1></caption><table class='flexy' ><thead><tr><td>Crime</td><td>Quadrilhas</td></tr></thead><tbody>";
								var body = '';
								var footer = "</tbody></table></div>"
								for (var i in data) {
									var quadrilhas = data[i].quadrilhas;
									var stringCrime = "("+data[i].crime.x+", "+data[i].crime.y+")";
									body += "<tr><td>"+stringCrime+"</td><td>"+quadrilhas.length+"</td><tr>"
								}
								var table = head+body+footer;
								$(this).html(table);
							}
						});
					});
				});
			});
		}


		var drawPoint = function(dist, num) {
			var c = document.getElementById("myCanvas");
			var ctx = c.getContext("2d");
			ctx.font = "20px Arial";
			ctx.beginPath();
			if (num===0) {
				ctx.fillStyle = "#772222";
				ctx.arc(dist.x+pointSize,dist.y+pointSize,pointSize,0,2*Math.PI);
			} else {
				if (id == 0) {
					id = dist.quadrilha.id;
					color = "#007798";
					ctx.fillStyle = color;
				} else if (id == dist.quadrilha.id) {
					ctx.fillStyle = color;
				} else {
					ctx.fillStyle = "#0f4";
				}
				ctx.fill();
				ctx.arc(dist.ponto.x+pointSize,dist.ponto.y+pointSize,pointSize,0,2*Math.PI);
				ctx.closePath();
				ctx.beginPath();
				ctx.fillStyle = "#000";
				ctx.fillText(num,dist.ponto.x+pointSize,dist.ponto.y+pointSize);
			}
			ctx.fill();
			ctx.closePath();
			ctx.stroke();
		}


		var investigarCrime = function(){
			$.ajax({
				url: "./investigacaoCore/investigarCrime",
				context: $("#myCanvas"),
				success: function (data){
					var c = document.getElementById("myCanvas");
					var ctx = c.getContext("2d");
        			ctx.clearRect(0, 0, 800, 600);
					drawPoint(data.crime, 0);
					for (var i in data.distancias){
						drawPoint(data.distancias[i], ++i);
					}
				}
			});
		};

		var limparBanco = function(){
			var c = document.getElementById("myCanvas");
			var ctx = c.getContext("2d");
			ctx.clearRect(0, 0, 800, 600);
			$.ajax({
				url: "./investigacaoCore/limparBanco",
				context: $("#page-footer"),
				success: function (){
					$(this).html("<div id='div-ponto' style='width:33%'></div><div id='div-quad' style='width:33%'></div><div id='div-crime' style='width:33%'></div>");
				}
			})
		};

		var newPonto = function(){
			$.ajax({
				url: "./ponto/create",
				context: $("#page-body"),
				success: function (data){
					$(this).html(data);
				}
			})
		};

		var savePonto = function() {
			$.ajax({
				url: "./ponto/save.json",
				method: "POST",
				context: $("#page-body"),
				data: {x:$("#pontoX").val(),y:$("#pontoY").val()},
				success: function (data){
					self.newPonto();
				}
			}).done(function() {
				$.ajax({
					url: "./ponto/index.json",
					method: "POST",
					context: $("#div-ponto"),
					success: function (data){
						var head = "<div style='width:100%'><caption><h1>Listagem de Pontos</h1></caption><table class='flexy' ><thead><tr><td>X</td><td>Y</td></tr></thead><tbody>";
						var body = '';
						var footer = "</tbody></table></div>"
						for (var i in data) {
							body += "<tr><td>"+data[i].x+"</td><td>"+data[i].y+"</td><tr>"
						}
						var table = head+body+footer;
						$(this).html(table);
					}
				});
			});
		};

		var newQuad = function(){
			$.ajax({
				url: "./quadrilha/create",
				context: $("#page-body"),
				success: function (data){
					$(this).html(data);
				}
			})
		};

		var saveQuad = function() {
			$.ajax({
				url: "./quadrilha/save.json",
				method: "POST",
				context: $("#page-body"),
				data: {pontos:$("#quad-pontos").val(),nome:$("#quad-nome").val()},
				success: function (data){
					self.newQuad();
				}
			}).done(function() {
				$.ajax({
					url: "./quadrilha/index.json",
					method: "POST",
					context: $("#div-quad"),
					success: function (data){
						var head = "<div style='width:100%'><caption><h1>Listagem de Quadrilhas</h1></caption><table class='flexy' ><thead><tr><td>Nome</td><td>Pontos</td></tr></thead><tbody>";
						var body = '';
						var footer = "</tbody></table></div>"
						for (var i in data) {
							var pontos = data[i].pontos;
							body += "<tr><td>"+data[i].nome+"</td><td>"+pontos.length+"</td><tr>"
						}
						var table = head+body+footer;
						$(this).html(table);
					}
				});
			});
		};

		var newCrime = function(){
			$.ajax({
				url: "./investigacaoCore/create",
				context: $("#page-body"),
				success: function (data){
					$(this).html(data);
				}
			})
		};

		var saveCrime = function() {
			$.ajax({
				url: "./investigacaoCore/save.json",
				method: "POST",
				context: $("#page-body"),
				data: {crime:$("#crime-crime").val(),quads:$("#crime-quad").val()},
				success: function (data){
					console.log(data);
					self.newCrime();
				},
				error: function (data){
					console.log('errors');
				}
			}).done(function() {
				$.ajax({
					url: "./investigacaoCore/index.json",
					method: "POST",
					context: $("#div-crime"),
					success: function (data){
						var head = "<div style='width:100%'><caption><h1>Listagem de Investigações</h1></caption><table class='flexy' ><thead><tr><td>Crime</td><td>Quadrilhas</td></tr></thead><tbody>";
						var body = '';
						var footer = "</tbody></table></div>"
						for (var i in data) {
							var quadrilhas = data[i].quadrilhas;
							var stringCrime = "("+data[i].crime.x+", "+data[i].crime.y+")";
							body += "<tr><td>"+stringCrime+"</td><td>"+quadrilhas.length+"</td><tr>"
						}
						var table = head+body+footer;
						$(this).html(table);
					}
				});
			});
		};

		</script>
	</body>
</html>
