package ordenacao

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.converters.JSON

@Transactional(readOnly = true)
class QuadrilhaController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    static scaffold = true

    def index() {
        JSON.use('deep'){
            render Quadrilha.list() as JSON
        }
    }

    def listByInvestigacao(){
        ArrayList<Quadrilha> quads = new ArrayList<Quadrilha>();
        ArrayList<InvestigacaoCore> inves = InvestigacaoCore.list()
        inves.each {
            quads.addAll(it.quadrilhas)
        }

        return (Quadrilha.list()-quads)
    }

    def show(Quadrilha quadrilhaInstance) {
        respond quadrilhaInstance
    }

    def create() {
        respond new Quadrilha(params), model:[quadrilhaList:listByInvestigacao()]
    }

    @Transactional
    def save(String nome) {
        Quadrilha quadrilhaInstance = new Quadrilha(nome:nome)
        def pontos = params.get("pontos[]")
        pontos.each {
            quadrilhaInstance.addToPontos(Ponto.findById(it))
        }

        if (quadrilhaInstance == null) {
            notFound()
            return
        }

        if (quadrilhaInstance.hasErrors()) {
            respond quadrilhaInstance.errors, view:'create'
            return
        }

        quadrilhaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'quadrilha.label', default: 'Quadrilha'), quadrilhaInstance.id])
                redirect quadrilhaInstance
            }
            '*' { respond quadrilhaInstance, [status: CREATED] }
        }
    }

    def edit(Quadrilha quadrilhaInstance) {
        respond quadrilhaInstance
    }

    @Transactional
    def update(Quadrilha quadrilhaInstance) {
        if (quadrilhaInstance == null) {
            notFound()
            return
        }

        if (quadrilhaInstance.hasErrors()) {
            respond quadrilhaInstance.errors, view:'edit'
            return
        }

        quadrilhaInstance.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Quadrilha.label', default: 'Quadrilha'), quadrilhaInstance.id])
                redirect quadrilhaInstance
            }
            '*'{ respond quadrilhaInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Quadrilha quadrilhaInstance) {

        if (quadrilhaInstance == null) {
            notFound()
            return
        }

        quadrilhaInstance.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Quadrilha.label', default: 'Quadrilha'), quadrilhaInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'quadrilha.label', default: 'Quadrilha'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
