package ordenacao

import grails.converters.JSON
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class InvestigacaoCoreController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def investigacaoCoreService
    static scaffold = true
    def index(Integer max) {
        JSON.use('deep'){
            render InvestigacaoCore.list() as JSON
        }
    }

    def limparBanco() {
        InvestigacaoCore.executeUpdate("delete from InvestigacaoCore p");
        Quadrilha.executeUpdate("delete from Quadrilha p");
        Ponto.executeUpdate("delete from Ponto p");

        JSON.use('deep'){
            render InvestigacaoCore.list() as JSON
        }
    }

    def investigarCrime() {
        InvestigacaoCore inves;

        def list = InvestigacaoCore.withCriteria{
            sqlRestriction " 1=1 order by rand()"
        }
        if (list) {
            inves = list.get(0)
        }
        JSON.use('deep'){
            render investigacaoCoreService.analisarCrime(inves) as JSON
        }

    }

    def autoIt(){
        investigacaoCoreService.novaInvestigacao()
    }

    def create() {
        respond new InvestigacaoCore(params)
    }

    @Transactional
    def save() {
        Ponto crime = Ponto.findById(params.crime)
        InvestigacaoCore investigacaoCoreInstance = new InvestigacaoCore(crime:crime, largura:800, altura:600)
        params.get("quads[]").each {
            investigacaoCoreInstance.addToQuadrilhas(Quadrilha.findById(it))
        }
        if (investigacaoCoreInstance == null) {
            notFound()
            return
        }

        if (investigacaoCoreInstance.hasErrors()) {
            respond investigacaoCoreInstance.errors, view:'create'
            return
        }

        investigacaoCoreInstance.save(flush:true)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'investigacaoCore.label', default: 'InvestigacaoCore'), investigacaoCoreInstance.id])
                redirect investigacaoCoreInstance
            }
            '*' { respond investigacaoCoreInstance, [status: CREATED] }
        }
    }
}
